
/**
 * WSReportarDireccionExcServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSReportarDireccionExcServiceSkeleton java skeleton for the axisService
     */
    public class WSReportarDireccionExcServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSReportarDireccionExcServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param codigoSistema
                                     * @param nombreUsuarioSolicitud
                                     * @param direccionNormalizada
                                     * @param direccionNatural
                                     * @param codigoDireccion
                                     * @param codigoPais
                                     * @param codigoDepartamento
                                     * @param codigoMunicipio
                                     * @param telefonos
                                     * @param contacto
                                     * @param estadoGeoreferenciacion
                                     * @param estadoPagina
                                     * @param instalacion
                                     * @param prioridad
                                     * @param comentario
                                     * @param direccionEstandarNLectura
         */
        

                 public co.net.une.www.gis.GisRespuestaGeneralType reportarDireccionExc
                  (
                  java.lang.String codigoSolicitud,java.lang.String codigoSistema,java.lang.String nombreUsuarioSolicitud,java.lang.String direccionNormalizada,java.lang.String direccionNatural,java.lang.String codigoDireccion,java.lang.String codigoPais,java.lang.String codigoDepartamento,java.lang.String codigoMunicipio,java.lang.String telefonos,java.lang.String contacto,java.lang.String estadoGeoreferenciacion,java.lang.String estadoPagina,java.lang.String instalacion,int prioridad,java.lang.String comentario,java.lang.String direccionEstandarNLectura
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("codigoSistema",codigoSistema);params.put("nombreUsuarioSolicitud",nombreUsuarioSolicitud);params.put("direccionNormalizada",direccionNormalizada);params.put("direccionNatural",direccionNatural);params.put("codigoDireccion",codigoDireccion);params.put("codigoPais",codigoPais);params.put("codigoDepartamento",codigoDepartamento);params.put("codigoMunicipio",codigoMunicipio);params.put("telefonos",telefonos);params.put("contacto",contacto);params.put("estadoGeoreferenciacion",estadoGeoreferenciacion);params.put("estadoPagina",estadoPagina);params.put("instalacion",instalacion);params.put("prioridad",prioridad);params.put("comentario",comentario);params.put("direccionEstandarNLectura",direccionEstandarNLectura);
		try{
		
			return (co.net.une.www.gis.GisRespuestaGeneralType)
			this.makeStructuredRequest(serviceName, "reportarDireccionExc", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    