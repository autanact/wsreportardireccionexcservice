
/**
 * WSReportarDireccionExcServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSReportarDireccionExcServiceMessageReceiverInOut message receiver
        */

        public class WSReportarDireccionExcServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSReportarDireccionExcServiceSkeleton skel = (WSReportarDireccionExcServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("reportarDireccionExc".equals(methodName)){
                
                co.net.une.www.gis.WSReportarDireccionExcRS wSReportarDireccionExcRS18 = null;
	                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedParam =
                                                             (co.net.une.www.gis.WSReportarDireccionExcRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.gis.WSReportarDireccionExcRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSReportarDireccionExcRS18 =
                                                   
                                                   
                                                           wrapWSReportarDireccionExcRSGisRespuestaProceso(
                                                       
                                                        

                                                        
                                                       skel.reportarDireccionExc(
                                                            
                                                                getCodigoSolicitud(wrappedParam)
                                                            ,
                                                                getCodigoSistema(wrappedParam)
                                                            ,
                                                                getNombreUsuarioSolicitud(wrappedParam)
                                                            ,
                                                                getDireccionNormalizada(wrappedParam)
                                                            ,
                                                                getDireccionNatural(wrappedParam)
                                                            ,
                                                                getCodigoDireccion(wrappedParam)
                                                            ,
                                                                getCodigoPais(wrappedParam)
                                                            ,
                                                                getCodigoDepartamento(wrappedParam)
                                                            ,
                                                                getCodigoMunicipio(wrappedParam)
                                                            ,
                                                                getTelefonos(wrappedParam)
                                                            ,
                                                                getContacto(wrappedParam)
                                                            ,
                                                                getEstadoGeoreferenciacion(wrappedParam)
                                                            ,
                                                                getEstadoPagina(wrappedParam)
                                                            ,
                                                                getInstalacion(wrappedParam)
                                                            ,
                                                                getPrioridad(wrappedParam)
                                                            ,
                                                                getComentario(wrappedParam)
                                                            ,
                                                                getDireccionEstandarNLectura(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSReportarDireccionExcRS18, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSReportarDireccionExcRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSReportarDireccionExcRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSReportarDireccionExcRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSReportarDireccionExcRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.gis.WSReportarDireccionExcRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.gis.WSReportarDireccionExcRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private java.lang.String getCodigoSolicitud(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoSolicitud();
                            
                        }
                     

                        private java.lang.String getCodigoSistema(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoSistema();
                            
                        }
                     

                        private java.lang.String getNombreUsuarioSolicitud(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getNombreUsuarioSolicitud();
                            
                        }
                     

                        private java.lang.String getDireccionNormalizada(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getDireccionNormalizada();
                            
                        }
                     

                        private java.lang.String getDireccionNatural(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getDireccionNatural();
                            
                        }
                     

                        private java.lang.String getCodigoDireccion(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoDireccion();
                            
                        }
                     

                        private java.lang.String getCodigoPais(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoPais();
                            
                        }
                     

                        private java.lang.String getCodigoDepartamento(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoDepartamento();
                            
                        }
                     

                        private java.lang.String getCodigoMunicipio(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getCodigoMunicipio();
                            
                        }
                     

                        private java.lang.String getTelefonos(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getTelefonos();
                            
                        }
                     

                        private java.lang.String getContacto(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getContacto();
                            
                        }
                     

                        private java.lang.String getEstadoGeoreferenciacion(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getEstadoGeoreferenciacion();
                            
                        }
                     

                        private java.lang.String getEstadoPagina(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getEstadoPagina();
                            
                        }
                     

                        private java.lang.String getInstalacion(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getInstalacion();
                            
                        }
                     

                        private int getPrioridad(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getPrioridad();
                            
                        }
                     

                        private java.lang.String getComentario(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getComentario();
                            
                        }
                     

                        private java.lang.String getDireccionEstandarNLectura(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSReportarDireccionExcRQ().getDireccionEstandarNLectura();
                            
                        }
                     
                        private co.net.une.www.gis.WSReportarDireccionExcRQType getreportarDireccionExc(
                        co.net.une.www.gis.WSReportarDireccionExcRQ wrappedType){
                            return wrappedType.getWSReportarDireccionExcRQ();
                        }
                        
                        
                    

                        
                        private co.net.une.www.gis.WSReportarDireccionExcRS wrapWSReportarDireccionExcRSGisRespuestaProceso(
                        co.net.une.www.gis.GisRespuestaGeneralType param){
                        co.net.une.www.gis.WSReportarDireccionExcRS wrappedElement = new co.net.une.www.gis.WSReportarDireccionExcRS();
                        co.net.une.www.gis.WSReportarDireccionExcRSType innerType = new co.net.une.www.gis.WSReportarDireccionExcRSType();
                                innerType.setGisRespuestaProceso(param);
                                wrappedElement.setWSReportarDireccionExcRS(innerType);
                            
                            return wrappedElement;
                        }
                     
                         private co.net.une.www.gis.WSReportarDireccionExcRS wrapreportarDireccionExc(
                            co.net.une.www.gis.WSReportarDireccionExcRSType innerType){
                                co.net.une.www.gis.WSReportarDireccionExcRS wrappedElement = new co.net.une.www.gis.WSReportarDireccionExcRS();
                                wrappedElement.setWSReportarDireccionExcRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.gis.WSReportarDireccionExcRQ.class.equals(type)){
                
                           return co.net.une.www.gis.WSReportarDireccionExcRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.gis.WSReportarDireccionExcRS.class.equals(type)){
                
                           return co.net.une.www.gis.WSReportarDireccionExcRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    