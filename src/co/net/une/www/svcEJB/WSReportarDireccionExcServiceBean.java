package co.net.une.www.svcEJB;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Servicios WEB GSS SmallWorld 4.3 - Gesti�n de Direcciones Excepcionadas
##	Archivo: WSReportarDireccionExcService.java
##	Contenido: Clase que contiene la implementaci�n del servicio WSReportarDireccionExcService
##	Autor: Freddy Molina
##  Fecha creaci�n: 13-12-2015
##	Fecha �ltima modificaci�n: 19-02-2016
##	Historial de cambios: 
##	19-02-2016	FJM		Primera versi�n
##	25/01/2016 FMS Creaci�n del servicio
##	27/01/2016 FMS Cambios en la direcci�n donde actualiza las tablas (respaldada 2016/01/27)
##	11/02/2016 FMS Se limpiaron los comentarios
##	11/02/2016 FMS Creaci�n de versi�n 1.0. Publicada en ambientes DESA/PRE/PROD  
##	13/02/2016 FMS Actualizaci�n versi�n 2.0  incluye estado E (v1.0 respaldada 2016/02/13)
##	19/02/2016 FMS Unificaci�n de estilo de documentaci�n (respaldada 2016/02/19)
##
##**********************************************************************************************************************************
*/

import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.gis.BoundedString14;
import co.net.une.www.gis.GisRespuestaGeneralType;
import co.net.une.www.gis.UTCDate;
import co.net.une.www.gis.WSReportarDireccionExcRSType;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.GisCommonInfoDirType;
import co.net.une.www.svc.WSRespuestaDEStub;

import com.gesmallworld.gss.lib.service.ServiceLocal;
import com.gesmallworld.gss.lib.service.magik.MagikService;
import com.gesmallworld.gss.lib.auth.AuthorisationException;
import com.gesmallworld.gss.lib.locator.ServiceLocator;
import com.gesmallworld.gss.lib.locator.ServiceLocatorException;
import com.gesmallworld.gss.lib.log.SWLog;
import com.gesmallworld.gss.lib.request.BusinessResponse;
import com.gesmallworld.gss.lib.request.ParameterException;
import com.gesmallworld.gss.lib.request.Request;

import javax.ejb.SessionBean;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Level;

import com.gesmallworld.gss.lib.request.Response;
import com.gesmallworld.gss.lib.service.magik.MagikService.StateHandling;

/**
 * Clase que contiene la implementaci�n del servicio WSReportarDireccionExcService
 * @author FreddyMolina
 * @creado 19/02/2016
 * @ultimamodificacion 19/02/2016
 * @version 2.0
 * @ejb.resource-ref
 *     res-type="javax.resource.cci.ConnectionFactory"
 *     res-auth="Container"
 *     res-ref-name="eis/SmallworldServer"
 *     jndi-name="eis/SmallworldServer"
 *     res-sharing-scope="Shareable"
 * @ejb.interface
 *     local-extends="javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal"
 *     package="co.net.une.www.interfaces"
 * @ejb.bean
 *     view-type="local"
 *     name="WSReportarDireccionExcService"
 *     type="Stateless"
 *     local-jndi-name="ejb/WSReportarDireccionExcServiceLocal"
 *     transaction-type="Bean"
 * @ejb.home
 *     local-extends="javax.ejb.EJBLocalHome"
 *     package="co.net.une.www.interfaces"
 */
@StateHandling(serviceProvider = "wsreportar_direccion_exc_service_service_provider")
public class WSReportarDireccionExcServiceBean extends MagikService implements SessionBean{

    /**
     * Local jndi name in use for this service bean.
     */
    public static final String LOCAL_JNDI_NAME = "ejb/WSReportarDireccionExcServiceLocal";

    /**
     * Serialisation ID.
     */
    private static final long serialVersionUID = 1L;
    
    /**
	 * Mensajes en properties
	 */
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.traducciones-ReportarDirExc", Locale.getDefault());
	//validaci�n gen�rica de errores GSS
	private static final String KeyGSSException = "validate.GSSException.code";
	//validaci�n gen�rica de errores
	private static final String KeyGeneralException = "validate.generalException";
	//validaci�n de par�metros repetidos
    private static final String KeyRequiredParameterException = "validate.required.parameterException";
    
    //nombre del servicio
    private static final String keyNombreServicio = rb.getString("servicio.nombre");
    //nombre de la funci�n
    private static final String keyNombreFuncion = rb.getString("servicio.nombrefuncion");
    
    //mensaje de error de par�metro inv�lido
	private String mensajeErrorParametroInvalido = "";
    
	private static final String keyFunctionName = "validate.functionName";
	private static final String keyInputException = "validate.throwInputException";
	private static final String KeyParameterException = "validate.parameterException";
	private static final String KeyServiceLocatorException = "validate.serviceLocatorException";
	private static final String KeyAuthorisationException = "validate.authorisationException";
	private static final String KeyGeoNoValido = "validate.estadogeo.error.novalido";
	private static final String KeyDirExcExiste = "validate.dirExcExiste";
	

    /**
     * Constructor.
     */
    public WSReportarDireccionExcServiceBean () {
        super();
    }
    
    /**
     * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
     * @ejb.interface-method
     * @param request A request object as specified in the service description file.
     * @return A response instance
     */
    @EISMapping(value = "reportar_direccion_exc")
    public Response reportarDireccionExc (Request request) {
    	
		//se configuran los par�metros de salida
		WSReportarDireccionExcRSType respuestaGssServiceEJB = new WSReportarDireccionExcRSType();
		
		GisRespuestaGeneralType respuestaGeneralGssServiceEJB = new GisRespuestaGeneralType();
		respuestaGssServiceEJB.setGisRespuestaProceso(respuestaGeneralGssServiceEJB);

		// Inicializamos los par�metros de respuesta del servicio
		inicializarParametrosSalida(respuestaGeneralGssServiceEJB);

		Response result = new BusinessResponse(request);
		try {

			// Obtengo todos los p�rametros de entradas obligatorios
			//obtengo el c�digo de solicitud
			String codigoSolicitud = (String) request.getParameter("codigoSolicitud");
			//obtengo el c�digo de sistema
			String codigoSistema = (String) request.getParameter("codigoSistema");
			//obtengo el nombre de usuario de la solicitud
			String nombreUsuarioSolicitud = (String) request.getParameter("nombreUsuarioSolicitud");
			//obtengo la direcci�n normalizada
			String direccionNormalizada = (String) request.getParameter("direccionNormalizada");
			//obtengo la direcci�n natural
			String direccionNatural = (String) request.getParameter("direccionNatural");
			//obtengo el c�digo de la direcci�n
			String codigoDireccion = (String) request.getParameter("codigoDireccion");
			//obtengo el c�digo del pa�s
			String codigoPais = (String) request.getParameter("codigoPais");
			//obtengo el c�digo del departamento
			String codigoDepartamento = (String) request.getParameter("codigoDepartamento");
			//obtengo el c�digo del municipio
			String codigoMunicipio = (String) request.getParameter("codigoMunicipio");
			//obtengo los tel�fonos
			String telefonos = (String) request.getParameter("telefonos");
			//obtengo el nombre del contacto
			String contacto = (String) request.getParameter("contacto");
			//obtengo el estado de reorrefenciaci�n
			String estadoGeoreferenciacion = (String) request.getParameter("estadoGeoreferenciacion");
			//obtengo el estado de p�gina
			String estadoPagina = (String) request.getParameter("estadoPagina");
			//obtengo la instalaci�n
			String instalacion = (String) request.getParameter("instalacion");
			//obtengo la prioridad
			Integer prioridad = (Integer) request.getParameter("prioridad");
			//obtengo el comentario
			String comentario = (String) request.getParameter("comentario");
			//obtengo la direcci�n NLectura
			String direccionEstandarNLectura = (String) request.getParameter("direccionEstandarNLectura");
			
			//valido los par�metros de entrada
			//codigoSolicitud es obligatorio
			validarCampoObligatorio("codigoSolicitud", codigoSolicitud, respuestaGssServiceEJB.getGisRespuestaProceso());
			//codigoSistema es obligatorio
			validarCampoObligatorio("codigoSistema", codigoSistema, respuestaGssServiceEJB.getGisRespuestaProceso());
			//nombreUsuarioSolicitud es obligatorio
			validarCampoObligatorio("nombreUsuarioSolicitud", nombreUsuarioSolicitud, respuestaGssServiceEJB.getGisRespuestaProceso());
			//direccionNormalizada es obligatorio
			validarCampoObligatorio("direccionNormalizada", direccionNormalizada, respuestaGssServiceEJB.getGisRespuestaProceso());
			//codigoPais es obligatorio
			validarCampoObligatorio("codigoPais", codigoPais, respuestaGssServiceEJB.getGisRespuestaProceso());
			//codigoDepartamento es obligatorio
			validarCampoObligatorio("codigoDepartamento", codigoDepartamento, respuestaGssServiceEJB.getGisRespuestaProceso());			
			//codigoMunicipio es obligatorio
			validarCampoObligatorio("codigoMunicipio", codigoMunicipio, respuestaGssServiceEJB.getGisRespuestaProceso());				
			//telefonos es obligatorio
			validarCampoObligatorio("telefonos", telefonos, respuestaGssServiceEJB.getGisRespuestaProceso());			
			//contacto es obligatorio
			validarCampoObligatorio("contacto", contacto, respuestaGssServiceEJB.getGisRespuestaProceso());		
			//estadoGeoreferenciacion es obligatorio
			validarCampoObligatorio("estadoGeoreferenciacion", estadoGeoreferenciacion, respuestaGssServiceEJB.getGisRespuestaProceso());	
			//prioridad es obligatorio
			validarCampoObligatorio("prioridad", prioridad, respuestaGssServiceEJB.getGisRespuestaProceso());	
			//direccionNatural es obligatorio
			validarCampoObligatorio("direccionNatural", direccionNatural, respuestaGssServiceEJB.getGisRespuestaProceso());		
			//codigoDireccion es obligatorio
			validarCampoObligatorio("codigoDireccion", codigoDireccion, respuestaGssServiceEJB.getGisRespuestaProceso());					
			
			//reviso el formato de los tel�fonos
			if( (telefonos != null) && (!"".equals(telefonos)) )
				telefonos=telefonos.replaceAll(";", ",");
			
			//identifica si la direcci�n fue ingresada satisfactoriamente en SW
			boolean ingresada = false;
			
			System.out.println("Desde reportar recibo estadoGeoreferenciacion: "+estadoGeoreferenciacion);
			
			//Los estados de geo permitidos son YBEC
			if ("YBEC".indexOf(estadoGeoreferenciacion)>=0){
				//valido si la direcc�n Excepcionada ya existe
				if (!siDireccionExiste(request,respuestaGeneralGssServiceEJB,codigoSolicitud)){
					
					//la direcci�n Excepcionada no existe se puede reportar
					System.out.println("codigoSolicitud: "+codigoSolicitud+" NO repetido se puede reportar");
					
					//obtengo la informaci�n de georreferencia de la direcci�n
					GisCommonInfoDirType dirGeo = ejecutarGeorrefencia(request, respuestaGeneralGssServiceEJB, codigoDepartamento, codigoMunicipio, direccionNatural,
							 codigoPais, "0", "0");
					
					//obtengo el estado de georreferencia
					String WSEstadoGeorreferenciacion = dirGeo.getEstadoGeoreferenciacion();
					System.out.println("Luego de Georreferenciar se obtiene: "+WSEstadoGeorreferenciacion);
		
					if (estadoGeoreferenciacion.equals(WSEstadoGeorreferenciacion)) {
						//C�mo es estado GEO es igual antes y despues de Georrefenciar, solo guardo sin enviar a CRM
						System.out.println("C�mo es estado GEO es igual antes y despues de Georrefenciar, solo guardo sin enviar a CRM");
						// Se ingresa la direcci�n excepcionada
						ejecutarBeanMagic(request, respuestaGeneralGssServiceEJB, codigoSolicitud, codigoSistema, nombreUsuarioSolicitud,
								direccionNormalizada, codigoPais, codigoDepartamento, codigoMunicipio,telefonos, estadoGeoreferenciacion, contacto, 
								instalacion, prioridad,	comentario, direccionEstandarNLectura, estadoPagina, codigoDireccion, direccionNatural);
						ingresada = true;
					} else {
						//C�mo es estado GEO es Diferente antes y despues de Georrefenciar, analizo
						System.out.println("C�mo es estado GEO es Diferente antes y despues de Georrefenciar, analizo");
						if (("E,J, C, F, G, B").indexOf(estadoGeoreferenciacion) >= 0) {
							//es un estado inv�lido analizo
							System.out.println("C�mo el estatus de GEO desde par�metros es:"+estadoGeoreferenciacion+" Sigo analizando");
							if (("MNYBE").indexOf(WSEstadoGeorreferenciacion) >= 0) {
								
								//es un estado v�lido analizo
								if (("YBE").indexOf(WSEstadoGeorreferenciacion) >= 0) {
									
									// C�mo el estatus despues de Georefenciar es YBE env�o a CRM y guardo la direcci�n
									System.out.println("C�mo el estatus despues de Georefenciar es:"+WSEstadoGeorreferenciacion);
									System.out.println("Env�o a CRM");
									
									// Se env�a al servicio RespuestaDE
									ejecutarRespuestaDE(request, respuestaGeneralGssServiceEJB, codigoSolicitud, codigoSistema, 
											codigoPais, codigoDepartamento, codigoMunicipio, instalacion, 
											direccionEstandarNLectura,estadoPagina, dirGeo.getCodigoDireccion(),dirGeo);
									
									System.out.println("Guardo la direci�n");
									// Se ingresa la direcci�n excepcionada
									ejecutarBeanMagic(request, respuestaGeneralGssServiceEJB, codigoSolicitud, codigoSistema,nombreUsuarioSolicitud, direccionNormalizada, 
											codigoPais, codigoDepartamento, codigoMunicipio, telefonos, WSEstadoGeorreferenciacion,contacto, instalacion, prioridad, 
											comentario, direccionEstandarNLectura,estadoPagina, dirGeo.getCodigoDireccion(), direccionNatural);
									
									ingresada = true;
								}
	
								if (("MN").indexOf(WSEstadoGeorreferenciacion) >= 0) {
									System.out.println("C�mo el estatus despues de Georefenciar es:"+WSEstadoGeorreferenciacion);
									System.out.println("Guardo la direci�n");
									// Se env�a al servicio RespuestaDE
									ejecutarRespuestaDE(request, respuestaGeneralGssServiceEJB, codigoSolicitud, codigoSistema,  
											codigoPais, codigoDepartamento, codigoMunicipio, instalacion, 
											 direccionEstandarNLectura,estadoPagina, dirGeo.getCodigoDireccion(), dirGeo);
									
									ingresada = true;
								}
							}else{
								//System.out.println("El estado de Geo es invalido, mando error");
								setErrorProcess(respuestaGeneralGssServiceEJB, KeyGeoNoValido, estadoGeoreferenciacion);
							}
						}else{
							//System.out.println("El estado de Geo es invalido, mando error");
							setErrorProcess(respuestaGeneralGssServiceEJB, KeyGeoNoValido, estadoGeoreferenciacion);
						}
					}
	
					// Se termina el proceso sat�sfactoriamente
					if (ingresada){
						setExitoProcessResponde(respuestaGeneralGssServiceEJB);
						System.out.println("El proceso se complet� Satisfactoriamente");
					}else{
						System.out.println("El proceso NO termin�");
					}
				}else{
					//System.out.println("El estado de Geo es invalido, mando error");
					setErrorProcess(respuestaGeneralGssServiceEJB, KeyDirExcExiste, "");
				}
			}else{
				//System.out.println("El estado de Geo es invalido, mando error");
				setErrorProcess(respuestaGeneralGssServiceEJB, KeyGeoNoValido, estadoGeoreferenciacion);
			}
		} catch (ParameterException e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyParameterException, keyFunctionName);
			e.printStackTrace();
		} catch (ServiceLocatorException e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyServiceLocatorException, keyFunctionName);
			e.printStackTrace();
		} catch (AuthorisationException e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyAuthorisationException, keyFunctionName);
			e.printStackTrace();
		}catch(SocketTimeoutException e){ 
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyGeneralException, keyFunctionName, e);
		}catch (Exception e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyGeneralException, keyFunctionName, e);
			e.printStackTrace();
		} finally {
			try {
				result.addResponse("response", respuestaGeneralGssServiceEJB);
			} catch (ParameterException e) {
				SWLog.log(Level.ERROR, this, e.getMessage());
			}
		}
		return result;
	}

    
	/**
	 * Funci�n que valida si el campo es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param valorCampo
	 * 			Valor del campo a validar
	 * @param gisRespuestaProceso
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, String valorCampo,	GisRespuestaGeneralType gisRespuestaProceso) throws ParameterException {
		// valido que el campo no sea vac�o
		if (ServiciosUtil.esCampoVacio(valorCampo)) {
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(gisRespuestaProceso, nombreCampo);
		}	
		
	}

	/**
	 * Funci�n que valida si el campo es obligatorio. No puede ser vac�o. Utilizado para n�meros enteros
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param valorCampo
	 * 			Valor del campo a validar
	 * @param gisRespuestaProceso
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, Integer valorCampo, GisRespuestaGeneralType gisRespuestaProceso) throws ParameterException {
		// valido que el campo no sea vac�o
		if (ServiciosUtil.esCampoVacio(valorCampo)) {
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(gisRespuestaProceso, nombreCampo);
		}	
		
	}
	/**
	 * Procedimiento que notifica la ausencia de un par�metro obligatorio en la entrada del servicio
	 * @param gisRespuestaProceso
	 * 			Objeto donde se retorna la respuesta
	 * @param nombreCampo
	 * 			Nombre del campo obligatorio ausente
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void notificarParametroObligatorio(GisRespuestaGeneralType gisRespuestaProceso, String nombreCampo) throws ParameterException {
		//configuro mensaje del log
		mensajeErrorParametroInvalido = MessageFormat.format(ServiciosUtil.getMensajeErrorParametros(),keyNombreServicio,nombreCampo);
		//el campo es vacio, lanzo excepcion
		registrarException(gisRespuestaProceso, rb.getString(KeyRequiredParameterException), MessageFormat.format(ServiciosUtil.getMensajeErrorParametrosObligatorios(), keyNombreFuncion), null, false);
		lanzarParameterException();
		
	}

	/**
	 * Lanzador de excepciones utilzado cuando fallas en la entrada de par�metros de los servicios
	 * @throws ParameterException
	 * 				Informa de la falla ocurrida
	 */
	private static void lanzarParameterException() throws ParameterException{
		Throwable t = new Throwable();
		ParameterException e = new ParameterException(t);
		throw e;
	}

	/**
	 * Funci�n que registra la excepci�n en los par�metros de salida
	 * @param gisRespuestaProceso
	 * 			Objeto de respuesta del proceso del servicio
	 * @param codigoParam
	 * 			Mensaje del c�digo de error
	 * @param mensaje
	 * 			Mensaje del error
	 * @param e
	 * 			Excepcion a reportar
	 * @param mostrarTrace
	 * 			Verdadero para mostrar el traceRoot de la Excepcion, falso en cualquier otro caso
	 */
	private void registrarException(GisRespuestaGeneralType gisRespuestaProceso, String codigoParam, String mensaje, Exception e, boolean mostrarTrace){

		//Lleno los par�metros de respuesta
		gisRespuestaProceso.setCodigoError(codigoParam);
		gisRespuestaProceso.setDescripcionError(mensaje);
		
		//Si mostrarTrace es TRUE se muestra el traceRoot
		if (mostrarTrace) e.printStackTrace();
		
	}

	/**
	 * Funci�n que valida si la direcci�n excepcionada ya existe.
	 * @param request
	 * 			requets del servicio
	 * @param respuestaGeneralGssServiceEJB
	 * 			Respuesta del proceso
	 * @param codigoSolicitud
	 * 			C�digo de solicitud de la direcci�n excepcionada
	 * @return
	 * 			Verdadero si la direcci�n excepcionada ya existe registrada. Falso en otro casp
	 * @throws Exception
	 */
	private boolean siDireccionExiste(Request request, GisRespuestaGeneralType respuestaGeneralGssServiceEJB,String codigoSolicitud) throws Exception {
		
		// Se genera la llamada encadenada
		Request r = request.createChainedRequest("ejb/ConsultarTablasGDELocal", "consultarTablasGDE");
		//variable que indica si la direcci�n existe
		boolean existeDireccion = true;
		try {
			
			// Se asignan los par�metros de entrada
			//asigno el nombre de la tabla
			r.setParameter("nombreTabla", "direccion_excepcionada");
			//asigno el dataset
			r.setParameter("nombreDataset", "une_st");
			//asigno el par�metro de consulta
			r.setParameter("nombresCamposConsulta", "codigo_solicitud");
			//asigno el valor del par�metro a consultar
			r.setParameter("valoresCamposConsulta", codigoSolicitud);
			//asigno los valores a retornar
			r.setParameter("nombreCamposResultado", "codigo_solicitud");

			// Se realiza la petici�n
			ServiceLocal s = (ServiceLocal) ServiceLocator.getInstance().getSLSB("ejb/ConsultarTablasGDELocal");
			Response gssServiceResponse = s.makeRequest(r);
			
			//obtengo la respuesta del servicio
			Map<String, Object> mapConsultarDirExc = gssServiceResponse.getResponses();

			//obtengo el codigo de respuesta
			String codigoRespuesta = (String) mapConsultarDirExc.get("codigoRespuesta");
			
			
			//eval�o la respuesta
			if (ServiciosUtil.OK.equals(codigoRespuesta)) {
				//el servicio se ejecut� sin problemas
				//obtengo los resultados de la consulta
				String resultados = (String) mapConsultarDirExc.get("registros");
				// Se obtienen los resultados
				if (ServiciosUtil.SINRESULTADOS.equals(resultados)){
					//La direcci�n NO EXISTE
					existeDireccion = false;
				}else{
					//reporto el que ya existe la direcci�n 
					respuestaGeneralGssServiceEJB.setDescripcionError(rb.getString(KeyDirExcExiste));
					respuestaGeneralGssServiceEJB.setCodigoError(rb.getString(KeyDirExcExiste + ".code"));
				}
			}
	
		} catch (Exception e) {
			// Se lanza una excepci�n
			throw new Exception("Error en servicio siDireccionExiste: " + e.getMessage());
		}
		//retorno si la direcci�n excepcionada existe
		return existeDireccion;
	}


	/**
	 * Funci�n que inicializa los par�metros de salida del servicio
	 * @param resp
	 * 			Par�metros de salia a inicializar
	 */
	private void inicializarParametrosSalida(GisRespuestaGeneralType resp) {
		//inicializo c�digo de respuesta, por defecto es falla
		resp.setCodigoRespuesta(ServiciosUtil.KO);
		//inicializo c�digo de Error
		resp.setCodigoError("");
		//inicializo descripci�n del error
		resp.setDescripcionError("");

		//creo la fecha de respuesta, por defecto la fecha del sistema
		UTCDate utcDate = new UTCDate();
		//obtengo la fecha del sistema
		Date ahora = new Date();
		//le doy formato la fecha del sistema
		SimpleDateFormat format = new SimpleDateFormat(ServiciosUtil.getFormatoFechaDefault());
		BoundedString14 boundedString14 = new BoundedString14();
		boundedString14.setBoundedString14(format.format(ahora.getTime()));
		utcDate.setDate(boundedString14);

		//asigno la fecha
		resp.setFechaRespuesta(utcDate);

	}

	private void setErrorProcessResponse(GisRespuestaGeneralType resp, Map<String, Object> mapRespuesta) throws Exception {
		resp.setCodigoError((String) mapRespuesta.get("codigoError") == null ? "" : (String) mapRespuesta.get("codigoError"));
		resp.setDescripcionError((String) mapRespuesta.get("descripcionError"));
		throw new Exception(rb.getString(keyInputException));
	}

	private void setErrorProcessResponse(GisRespuestaGeneralType resp, String keyMessage, String value) {
		resp.setCodigoError(rb.getString(keyMessage + ".code"));
		resp.setDescripcionError(MessageFormat.format(rb.getString(keyMessage), rb.getString(value)));
	}
	
	private void setErrorProcess(GisRespuestaGeneralType resp, String keyMessage, String value) {
		resp.setCodigoError(rb.getString(keyMessage + ".code"));
		resp.setDescripcionError(MessageFormat.format(rb.getString(keyMessage), value));
	}

	private void setErrorProcessResponse(GisRespuestaGeneralType resp, String keyMessage, String value, Exception e) {
		resp.setCodigoError(rb.getString(keyMessage + ".code"));
		resp.setDescripcionError(MessageFormat.format(rb.getString(keyMessage), rb.getString(value)) + ": " + e.getMessage());
		e.printStackTrace();
	}

	private void setExitoProcessResponde(GisRespuestaGeneralType respuestaGeneralGssServiceEJB) {
		respuestaGeneralGssServiceEJB.setCodigoRespuesta("OK");
		respuestaGeneralGssServiceEJB.setCodigoError("");
		respuestaGeneralGssServiceEJB.setDescripcionError("");

	}

	/**
	 * Funci�n que retorna la informaci�n de una direcci�n georreferenciada.
	 * @param request
	 * 			request del servicio
	 * @param respuestaGeneralGssServiceEJB
	 * 			respuesta del proceso
	 * @param codigoDepartamento
	 * 			c�digo del departamento
	 * @param codigoMunicipio
	 * 			c�digo del municipio
	 * @param direccionNatural
	 * 			direcci�n natural
	 * @param codigoPais
	 * 			c�digo del pa�s
	 * @param latitud
	 * 			latitud de la direcci�n
	 * @param longitud
	 * 			longitud de la direcci�n
	 * @return 
	 * 			Informaci�n de la direcci�n georeferenciada
	 * @throws RemoteException
	 */
	private  GisCommonInfoDirType ejecutarGeorrefencia(Request request, GisRespuestaGeneralType respuestaGeneralGssServiceEJB, String codigoDepartamento, String codigoMunicipio, String direccionNatural,
			String codigoPais, String latitud, String longitud) throws RemoteException {

		//proxy que comunica con servicio de georeferencia
		WSGeorreferenciarCRServiceStub geoProxy;

		try {
			//se configuran las variable de conexi�n con el servicio de georreferencia
			geoProxy = new WSGeorreferenciarCRServiceStub();
			WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQ geoRequest = new WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQ();
			WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQType geoRequestType = new WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQType();
			WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRS geoResponse = new WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRS();

			//asigno codigoDepartamento al servicio de georreferencia
			geoRequestType.setCodigoDepartamento(codigoDepartamento);
			//asigno codigoMunicipio al servicio de georreferencia
			geoRequestType.setCodigoMunicipio(codigoMunicipio);
			//asigno codigoPais al servicio de georreferencia
			geoRequestType.setCodigoPais(codigoPais);
			//asigno direccionNatural al servicio de georreferencia
			geoRequestType.setDireccionNatural(direccionNatural);
			//asigno latitud al servicio de georreferencia
			geoRequestType.setLatitud(latitud);
			//asigno longitud al servicio de georreferencia
			geoRequestType.setLongitud(longitud);
			//asigno la banera de Direcci�n Excepcionada al servicio de georreferencia. POR DEFECTO ES 0
			geoRequestType.setUNE_Cobertura_Especial(0);

			//asigino los par�metros de entrada al servicio
			geoRequest.setWSGeorreferenciarCRRQ(geoRequestType);
			
			//invoco el servicio de Georreferencia
			geoProxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			
			//obtengo la respuesta del servivio
			geoResponse = geoProxy.georeferenciarCR(geoRequest);
			//obtengo el codigo de respuesta
			String codigoRespuesta = geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoRespuesta();

			//eval�o el c�digo de respuesta
			if (ServiciosUtil.KO.equals(codigoRespuesta)) {
				//Se present� un error de comunicaci�n, lanzo una excepci�n
				throw new RemoteException(geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getDescripcionError());

			} else {
				// obtengo la informaci�n de la direcci�n
				return geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir();

			}
			
		} catch (AxisFault e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyParameterException, keyFunctionName);
			throw new RemoteException("Error en los par�metros de entrada: WSGeorreferenciar");
		} catch (RemoteException e) {
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, KeyParameterException, keyFunctionName);
			throw new RemoteException("Error de comunicaci�n con el servicio WSGeorreferenciar: "+e.getMessage());
		}
	}


	/**
	 * M�todo que registra la direcci�n excepcionada dentro de SW
	 * @param request
	 * 			respuesta del servicio
	 * @param respuestaGeneralGssServiceEJB
	 * 			respuesta del proceso
	 * @param codigoSolicitud
	 * 			c�digo de la solitud
	 * @param codigoSistema
	 * 			c�digo del sistema
	 * @param nombreUsuarioSolicitud
	 * 			nombre del usuario de la solicitud
	 * @param direccionNormalizada
	 * 			direcci�n normalizada
	 * @param codigoPais
	 * 			c�digo del pa�s
	 * @param codigoDepartamento
	 * 			c�digo del departamento
	 * @param codigoMunicipio
	 * 			c�digo del municipio
	 * @param telefonos
	 * 			tel�fonos del contacto
	 * @param estadoGeoreferenciacion
	 * 			estado de georreferenciaci�n
	 * @param contacto
	 * 			contacto de la solicitud
	 * @param instalacion
	 * 			instalaci�n
	 * @param prioridad
	 * 			prioridad
	 * @param comentario
	 * 			comentario
	 * @param direccionEstandarNLectura
	 * 			direcci�n est�ndar NLectura
	 * @param estadoPagina
	 * 			estado p�gina
	 * @param codigoDireccion
	 * 			c�digo de la direcci�n
	 * @param direccionNatural
	 * 			direcci�n natural
	 * @throws Exception
	 * 			lanza una excepci�n cuando se presenta alg�n error
	 */
	private void ejecutarBeanMagic(Request request, GisRespuestaGeneralType respuestaGeneralGssServiceEJB, String codigoSolicitud, String codigoSistema, String nombreUsuarioSolicitud,
			String direccionNormalizada, String codigoPais, String codigoDepartamento, String codigoMunicipio, String telefonos, String estadoGeoreferenciacion, String contacto, String instalacion,
			Integer prioridad, String comentario, String direccionEstandarNLectura, String estadoPagina, String codigoDireccion, String direccionNatural) throws Exception {

		//defino el EJB y la funci�n a ejecutar
		Request r = request.createChainedRequest("ejb/ReportarDireccionExcActLocal", "reportarDireccionExc");

		//asigno el conjunto de par�metros
		r.setParameter("codigoSolicitud", codigoSolicitud);
		r.setParameter("codigoSistema", codigoSistema);
		r.setParameter("nombreUsuarioSolicitud", nombreUsuarioSolicitud);
		r.setParameter("direccionNormalizada", direccionNormalizada);
		r.setParameter("codigoPais", codigoPais);
		r.setParameter("codigoDepartamento", codigoDepartamento);
		r.setParameter("codigoMunicipio", codigoMunicipio);
		r.setParameter("telefonos", telefonos);
		r.setParameter("contacto", contacto);
		r.setParameter("estadoGeoreferenciacion", estadoGeoreferenciacion);
		r.setParameter("instalacion", instalacion);
		r.setParameter("prioridad", prioridad.toString());
		r.setParameter("comentario", comentario);
		r.setParameter("direccionEstandarNLectura", direccionEstandarNLectura);
		r.setParameter("estadoPagina", estadoPagina);
		r.setParameter("codigoDireccion", codigoDireccion);
		r.setParameter("direccionNatural", direccionNatural);

		//defino el EJB
		ServiceLocal s = (ServiceLocal) ServiceLocator.getInstance().getSLSB("ejb/ReportarDireccionExcActLocal");
		Response gssServiceResponse = s.makeRequest(r);

		//obtengo la respuesta
		Map<String, Object> mapRespuestaDireccionExc = gssServiceResponse.getResponses();
		//obtengo el c�digo respuesta
		String codigoRespuesta = (String) mapRespuestaDireccionExc.get("codigoRespuesta");
		
		//valido el c�digo de respuesta
		if (ServiciosUtil.KO.equals(codigoRespuesta)) {
			//se present� un error en el registro, lanzo una excepci�n
			setErrorProcessResponse(respuestaGeneralGssServiceEJB, mapRespuestaDireccionExc);
			throw new Exception((String) mapRespuestaDireccionExc.get("descripcionError"));
		}

	}


	/**
	 * @param request
	 * 			respuesta del servicio
	 * @param respuestaGeneralGssServiceEJB
	 * 			respuesta del proceso
	 * @param codigoSolicitud
	 * 			c�digo de la solitud
	 * @param codigoSistema
	 * 			c�digo del sistema
	 * @param codigoPais
	 * 			c�digo del pa�s
	 * @param codigoDepartamento
	 * 			c�digo del departamento
	 * @param codigoMunicipio
	 * 			c�digo del municipio
	 * @param instalacion
	 * 			instalaci�n
	 * @param direccionEstandarNLectura
	 * 			direcci�n est�ndar NLectura
	 * @param estadoPagina
	 * 			estado p�gina
	 * @param codigoDireccion
	 * 			c�digo de la direcci�n
	 * @param dirGeo
	 * @throws Exception
	 */
	private void ejecutarRespuestaDE(Request request, GisRespuestaGeneralType respuestaGeneralGssServiceEJB, String codigoSolicitud, String codigoSistema, 
			String codigoPais, String codigoDepartamento, String codigoMunicipio,   String instalacion,
		    String direccionEstandarNLectura, String estadoPagina, String codigoDireccion, GisCommonInfoDirType dirGeo) throws Exception {

		
		//bandera para identificar si actualizo
		String codigoRespuesta = "OK";
		//Por defecto las direcciones que se env�an a RespuestaDE son exactas
		String estado_excepcion_type="EXACTA";
		
		//asigno los par�metros de entrada
		WSRespuestaDEStub proxy = new WSRespuestaDEStub();
		WSRespuestaDEStub.WSRespuestaDERQ requestLocal = new WSRespuestaDEStub.WSRespuestaDERQ();
		requestLocal.setWSRespuestaDERQ(new WSRespuestaDEStub.WSRespuestaDERQType());
		WSRespuestaDEStub.WSRespuestaDERS response = null;
		WSRespuestaDEStub.BoundedString1 estadoGeo;
		estadoGeo = new WSRespuestaDEStub.BoundedString1();
		estadoGeo.setBoundedString1(dirGeo.getEstadoGeoreferenciacion());
			
		WSRespuestaDEStub.BoundedString2 codDep,codPais,rural;
		codDep = new WSRespuestaDEStub.BoundedString2();
		codDep.setBoundedString2(codigoDepartamento);
		codPais = new WSRespuestaDEStub.BoundedString2();
		codPais.setBoundedString2(codigoPais);
		rural = new WSRespuestaDEStub.BoundedString2();
		rural.setBoundedString2(colocarMascara(dirGeo.getRural()));
		
		WSRespuestaDEStub.BoundedString5 estPag;
		estPag = new WSRespuestaDEStub.BoundedString5();
		estPag.setBoundedString5(colocarMascara(estadoPagina));
		
		
		WSRespuestaDEStub.BoundedString8 codMun;
		codMun = new WSRespuestaDEStub.BoundedString8();
		codMun.setBoundedString8(codigoMunicipio);
		
		
		WSRespuestaDEStub.BoundedString10 codigoBarrio,codigoComuna,codigoLocalizacionTipo1,placa;
		codigoBarrio = new WSRespuestaDEStub.BoundedString10();
		codigoBarrio.setBoundedString10(colocarMascara(dirGeo.getCodigoBarrio()));
		codigoComuna = new WSRespuestaDEStub.BoundedString10();
		codigoComuna.setBoundedString10(colocarMascara(dirGeo.getCodigoComuna()));
		codigoLocalizacionTipo1 = new WSRespuestaDEStub.BoundedString10();
		codigoLocalizacionTipo1.setBoundedString10(colocarMascara(dirGeo.getCodigoLocalizacionTipo1()));
		placa = new WSRespuestaDEStub.BoundedString10();
		placa.setBoundedString10(colocarMascara(dirGeo.getPlaca()));
				
		WSRespuestaDEStub.BoundedString15 csol = new WSRespuestaDEStub.BoundedString15();
		csol.setBoundedString15(codigoSolicitud);
				
		WSRespuestaDEStub.BoundedString14 codigoDaneManzana;
		codigoDaneManzana = new WSRespuestaDEStub.BoundedString14();
		codigoDaneManzana.setBoundedString14(colocarMascara(dirGeo.getCodigoDaneManzana()));
			
		WSRespuestaDEStub.BoundedString17 codigoPredio;
		codigoPredio = new WSRespuestaDEStub.BoundedString17();
		codigoPredio.setBoundedString17(colocarMascara(dirGeo.getCodigoPredio()));
			
		WSRespuestaDEStub.BoundedString18 insta;
		insta = new WSRespuestaDEStub.BoundedString18();
		insta.setBoundedString18(colocarMascara(instalacion));
		
		WSRespuestaDEStub.BoundedString20 tipoAgregacionNivel1;
		tipoAgregacionNivel1= new WSRespuestaDEStub.BoundedString20();
		tipoAgregacionNivel1.setBoundedString20(colocarMascara(dirGeo.getTipoAgregacionNivel1()));
			
		WSRespuestaDEStub.BoundedString30 csis = new WSRespuestaDEStub.BoundedString30();
		csis.setBoundedString30(codigoSistema);
		
		WSRespuestaDEStub.BoundedString30 ee = new WSRespuestaDEStub.BoundedString30();
		ee.setBoundedString30(estado_excepcion_type);
				
		WSRespuestaDEStub.BoundedString100 cdir,agregado,codigoDireccionProveevor,nombreBarrio,nombreComuna,nombreLocalizacionTipo1,remanente;
		cdir = new WSRespuestaDEStub.BoundedString100();
		cdir.setBoundedString100(codigoDireccion);
		codigoDireccionProveevor = new WSRespuestaDEStub.BoundedString100();
		codigoDireccionProveevor.setBoundedString100(colocarMascara(dirGeo.getCodigoDireccionProveevor()));
		agregado = new WSRespuestaDEStub.BoundedString100();
		agregado.setBoundedString100(colocarMascara(dirGeo.getAgregado()));
		nombreBarrio = new WSRespuestaDEStub.BoundedString100();
		nombreBarrio.setBoundedString100(colocarMascara(dirGeo.getNombreBarrio()));
		nombreComuna = new WSRespuestaDEStub.BoundedString100();
		nombreComuna.setBoundedString100(colocarMascara(dirGeo.getNombreComuna()));
		nombreLocalizacionTipo1 = new WSRespuestaDEStub.BoundedString100();
		nombreLocalizacionTipo1.setBoundedString100(colocarMascara(dirGeo.getNombreLocalizacionTipo1()));
		remanente = new WSRespuestaDEStub.BoundedString100();
		remanente.setBoundedString100(colocarMascara(dirGeo.getRemanente()));
		
		WSRespuestaDEStub.BoundedString250 direccionAnterior,dirEstandarNLectura,dirNorm;
		direccionAnterior = new WSRespuestaDEStub.BoundedString250();
		direccionAnterior.setBoundedString250(colocarMascara(dirGeo.getDireccionAnterior()));
		dirEstandarNLectura = new WSRespuestaDEStub.BoundedString250();
		dirEstandarNLectura.setBoundedString250(colocarMascara(direccionEstandarNLectura));
		dirNorm = new WSRespuestaDEStub.BoundedString250();
		dirNorm.setBoundedString250(colocarMascara(dirGeo.getDireccionNormalizada()));
			
		requestLocal.getWSRespuestaDERQ().setCodigoSolicitud(csol);
		requestLocal.getWSRespuestaDERQ().setCodigoSistema(csis);
		requestLocal.getWSRespuestaDERQ().setEstadoExcepcion(ee);
		requestLocal.getWSRespuestaDERQ().setCodigoDireccion(cdir);
			
		requestLocal.getWSRespuestaDERQ().setAgregado(agregado);
		requestLocal.getWSRespuestaDERQ().setCodigoBarrio(codigoBarrio);
		requestLocal.getWSRespuestaDERQ().setCodigoComuna(codigoComuna);
		requestLocal.getWSRespuestaDERQ().setCodigoDaneManzana(codigoDaneManzana);
		requestLocal.getWSRespuestaDERQ().setCodigoDepartamento(codDep);
		requestLocal.getWSRespuestaDERQ().setCodigoDireccionProveevor(codigoDireccionProveevor);
		requestLocal.getWSRespuestaDERQ().setCodigoLocalizacionTipo1(codigoLocalizacionTipo1);
		requestLocal.getWSRespuestaDERQ().setCodigoMunicipio(codMun);
		requestLocal.getWSRespuestaDERQ().setCodigoPais(codPais);
		requestLocal.getWSRespuestaDERQ().setCodigoPredio(codigoPredio);
		requestLocal.getWSRespuestaDERQ().setCoordenadaX(dirGeo.getCoordenadaX());
		requestLocal.getWSRespuestaDERQ().setCoordenadaY(dirGeo.getCoordenadaY());
		requestLocal.getWSRespuestaDERQ().setDireccionAnterior(direccionAnterior);
		requestLocal.getWSRespuestaDERQ().setDireccionEstandarNLectura(dirEstandarNLectura);
		requestLocal.getWSRespuestaDERQ().setDireccionNormalizada(dirNorm);
		requestLocal.getWSRespuestaDERQ().setEstadoGeoreferenciacion(estadoGeo);
		requestLocal.getWSRespuestaDERQ().setEstadoPagina(estPag);
		requestLocal.getWSRespuestaDERQ().setEstrato(dirGeo.getEstrato());
		requestLocal.getWSRespuestaDERQ().setInstalacion(insta);
		requestLocal.getWSRespuestaDERQ().setLatitud(dirGeo.getLatitud());
		requestLocal.getWSRespuestaDERQ().setLongitud(dirGeo.getLongitud());
		requestLocal.getWSRespuestaDERQ().setNombreBarrio(nombreBarrio);
		requestLocal.getWSRespuestaDERQ().setNombreComuna(nombreComuna);
		requestLocal.getWSRespuestaDERQ().setNombreLocalizacionTipo1(nombreLocalizacionTipo1);
		requestLocal.getWSRespuestaDERQ().setPlaca(placa);
		requestLocal.getWSRespuestaDERQ().setRemanente(remanente);
		requestLocal.getWSRespuestaDERQ().setRural(rural);
		requestLocal.getWSRespuestaDERQ().setTipoAgregacionNivel1(tipoAgregacionNivel1);
		proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
		proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(5 * 60 * 1000);
		response = proxy.respuestaDE(requestLocal);

		if (response !=null){
			
			//obtengo la respuesta del servicio
			codigoRespuesta= response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoRespuesta();
			//val�do la respuesta del servicoio
			if (ServiciosUtil.KO.equals(codigoRespuesta)) {
				//se present� un error registro la excepci�n y lanzo excepci�n
				respuestaGeneralGssServiceEJB.setCodigoError(response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoError() == null ? "" : response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoError());
				respuestaGeneralGssServiceEJB.setDescripcionError(response.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError());
				throw new Exception(rb.getString(keyInputException));
			}
		}else{
			respuestaGeneralGssServiceEJB.setCodigoError("");
			respuestaGeneralGssServiceEJB.setDescripcionError("Servicio WSRespuestaDE no respondio");
			throw new Exception("Servicio WSRespuestaDE no respondio");
		}
	}

	private String colocarMascara(String valor) {
		return valor==null? "":valor;
	}
}
